import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/utils/helper.dart';

class Body extends StatefulWidget {
  Body({ Key key }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String valueChoose;

  List listItem = [
    "Accidents",
    "Brulures",
    "Malaises",
    "Plaies"
  ];

  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController traitementController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        width: Helper.getScreenWidth(context),
        height: Helper.getScreenHeight(context),
        child: ListView(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blueGrey, width: 1),
                borderRadius: BorderRadius.circular(20),
              ),
              child: DropdownButton(
                underline: SizedBox(),
                icon: Icon(Icons.arrow_drop_down),
                iconSize: 30,
                isExpanded: true,
                hint: Text("Select Category"),
                value: valueChoose,
                onChanged: (value) {
                  setState(() {
                    valueChoose = value;
                  });
                },
                items: listItem.map((valueItem) {
                  return DropdownMenuItem(
                    value: valueItem,
                    child: Text(valueItem),
                  );
                }).toList(),
              ),
            ),
            SizedBox(height: 30),
            AddTextField(text: "Enter discomfort name", controller: nameController),
            SizedBox(height: 30),
            AddTextField(text: "Enter discomfort description", controller: descriptionController),
            SizedBox(height: 30),
            AddTextField(text: "Enter discomfort treatment", controller: traitementController),
            // SizedBox(height: 30),
            // AddTextField(text: "Submit"),
          ],
        ),
      ),
    );
  }

  addMalaise() async {
    if(valueChoose == "Accidents") {
      await FirebaseFirestore.instance.collection('types').doc("0VLIUGXCcILh91rmJNW0").set({'title': nameController.text, 'description': descriptionController.text, 'traitement': traitementController.text});
    }
    else if(valueChoose == "Brulures") {
      await FirebaseFirestore.instance.collection('types').doc("9O7KaV9GNBN5gX1eiQhX").set({'title': nameController.text, 'description': descriptionController.text, 'traitement': traitementController.text});
    }
    else if(valueChoose == "Malaises") {
      await FirebaseFirestore.instance.collection('types').doc("P52EgkVrppSyqm2ISTGM").set({'title': nameController.text, 'description': descriptionController.text, 'traitement': traitementController.text});
    }
    else if(valueChoose == "Plaies") {
      await FirebaseFirestore.instance.collection('types').doc("UYz5wG3jrSwA5fHS5RiV").set({'title': nameController.text, 'description': descriptionController.text, 'traitement': traitementController.text});
    }
  }
}

class AddTextField extends StatelessWidget {
  const AddTextField({
    @required String text,
    @required TextEditingController controller,
    Key key,
  }) : _text = text , _controller = controller , super(key: key);

  final String _text;
  final TextEditingController _controller;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      decoration: InputDecoration(
        hintText: _text,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
      ),
    );
  }
}