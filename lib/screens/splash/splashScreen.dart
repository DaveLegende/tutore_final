import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../category/category.dart';
import 'package:tutore/utils/helper.dart';

class SplashScreen extends StatefulWidget {
  static const String routeName = '/splashScreen';
  const SplashScreen({ Key key }) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();

    Timer(Duration(seconds: 10), () => Navigator.of(context).pushReplacementNamed(Category.routeName));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: Helper.getScreenWidth(context),
            height: Helper.getScreenHeight(context),
            child: Image.asset(
              Helper.getAssetName("splashIcon.png"),
              fit: BoxFit.fill,
            ),
          ),
          Center(
            child: SpinKitChasingDots(
              color: Colors.blueGrey,
            ),
          )
        ],
      ),
    );
  }
}