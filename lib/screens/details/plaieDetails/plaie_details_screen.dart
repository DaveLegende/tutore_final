import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:tutore/utils/constants.dart';
import 'components/body.dart';
import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class PlaieDetailScreen extends StatefulWidget {
  final int index;
  PlaieDetailScreen({this.index});

  @override
  _PlaieDetailScreenState createState() => _PlaieDetailScreenState();
}

class _PlaieDetailScreenState extends State<PlaieDetailScreen> with TickerProviderStateMixin{

  Animation<double> _animation;
  AnimationController _animationController;

  @override
  void initState(){
        
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 260),
    );

    final curvedAnimation = CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
    
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(index: widget.index),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      
      floatingActionButton: SpeedDial(
        overlayColor: Colors.transparent,
        backgroundColor: mPrimaryColor,
        animatedIcon: AnimatedIcons.menu_close,
        closeManually: true,
        children: [
          SpeedDialChild(
            backgroundColor: Colors.red,
            child: Icon(Icons.call, color: Colors.white),
            onTap: () {
              _callNumber();
            },
          ),
          SpeedDialChild(
            backgroundColor: Colors.yellow,
            child: Icon(Icons.play_arrow, color: Colors.white),
            onTap: () {},
          ),
        ]
      ),
      //Init Floating Action Bubble 
      // floatingActionButton: FloatingActionBubble(
      //   backGroundColor: mPrimaryColor,
      //   items: <Bubble>[
      //     Bubble(
      //       // title:"Settings",
      //       iconColor :Colors.white,
      //       bubbleColor : Colors.red,
      //       icon:Icons.call,
      //       titleStyle:TextStyle(fontSize: 16 , color: Colors.white),
      //       onPress: () {
      //         _animationController.reverse();
      //       },
      //     ),
      //     Bubble(
      //       // title:"Profile",
      //       iconColor :Colors.white,
      //       bubbleColor : Colors.blue,
      //       icon:Icons.play_arrow,
      //       titleStyle:TextStyle(fontSize: 16 , color: Colors.white),
      //       onPress: () {
      //         _animationController.reverse();
      //       },
      //     ),
      //   ],

      //   // animation controller
      //   animation: _animation,

      //   // On pressed change animation state
      //   onPress: _animationController.isCompleted
      //       ? _animationController.reverse
      //       : _animationController.forward,
        
      //   // Floating Action button Icon color
      //   iconColor: Colors.blue,
      // )
    );
  }

  _callNumber() async{
    const number = '+22898790720'; //set the number here
    bool res = await FlutterPhoneDirectCaller.callNumber(number);
  }
}
