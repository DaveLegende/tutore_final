// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:tutore/utils/constants.dart';
// import 'package:tutore/utils/helper.dart';
// import 'package:flutter_tts/flutter_tts.dart';

// class Body extends StatelessWidget {

//   final int index;
//   Body({this.index});

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
//       future: FirebaseFirestore.instance.collection("types").doc("P52EgkVrppSyqm2ISTGM").collection("malaise").get(),
//       builder: (context, snapshot) {
//         if(snapshot.connectionState == ConnectionState.waiting) {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         }

//         return CustomScrollView(
//           slivers: [
//             SliverAppBar(
//               pinned: true,
//               expandedHeight: 200,
//               flexibleSpace: FlexibleSpaceBar(
//                 centerTitle: true,
//                 title: Text(snapshot.data.docs[index]['title'], /*style: Helper.getTheme(context).headline3,*/),
//                 background: Container(
//                   height: 250,
//                   width: double.infinity,
//                   decoration: BoxDecoration(
//                     gradient: LinearGradient(
//                       begin: Alignment.bottomCenter,
//                       end: Alignment.topCenter,
//                       colors: [
//                         Colors.black.withOpacity(0.7),
//                         Colors.black.withOpacity(0.2),
//                       ],
//                     ),
//                   ),
//                   child: Image.asset(Helper.getAssetName("dessert.jpg"), fit: BoxFit.cover),
//                 ),
//               ),
//             ),
//             _buildWidget(snapshot, index)
//           ],
//         );
//       },
//     );
//   }

//   Future _speak(AsyncSnapshot snapshot, int i) async {
    
//     FlutterTts flutterTts = FlutterTts();
//     await flutterTts.setLanguage("fr-FR");
//     await flutterTts.setPitch(1);
//     print(await flutterTts.getVoices);
//     await flutterTts.speak(snapshot.data.docs[i]['desc']);
//   }
//   Widget _buildWidget(AsyncSnapshot snapshot, int i) => SliverToBoxAdapter(
//     child: ListView.builder(
//       primary: false,
//       shrinkWrap: true,
//       itemCount: 1,
//       // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
//       itemBuilder: (context, index) => Stack(
//         children: [
//           Container(
//             width: double.infinity,
//             decoration: BoxDecoration(
//               color: Colors.white,
//               boxShadow: [
//                 BoxShadow(
//                   color: placeholder,
//                   offset: Offset(0,5),
//                   blurRadius: 10
//                 )
//               ]
//             ),
//             child: Column(
//               children: [
//                 Container(
//                   child: Column(
//                     children: [
//                       Center(
//                         child: Container(
//                           width: Helper.getScreenWidth(context) / 2,
//                           height: 50,
//                           color: Colors.yellow,
//                           child: Center(child: Text("DESCRIPTION", style: Helper.getTheme(context).headline6))
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(20),
//                         child: Text(
//                           snapshot.data.docs[i]['desc'],
//                           style: Helper.getTheme(context).headline5,
//                           textAlign: TextAlign.justify,
//                           ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Container(
//                   child: Column(
//                     children: [
//                       Center(
//                         child: Container(
//                           width: Helper.getScreenWidth(context) / 2,
//                           height: 50,
//                           color: Colors.yellow,
//                           child: Center(child: Text("TRAITEMENTS", style: Helper.getTheme(context).headline6))
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(20),
//                         child: Text(
//                           snapshot.data.docs[i]['traitement'],
//                           style: Helper.getTheme(context).headline5,
//                           textAlign: TextAlign.justify,
//                           ),
//                       ),
//                     ],
//                   ),
//                 ),
//                 SizedBox(height: 100),
//               ],
//             ),
//           ),
//           Positioned(
//             bottom: 0,
//             right: 20,
//             child: IconButton(
//               color: Colors.yellow,
//               onPressed: () {
//                 _speak(snapshot, i);
//               },
//               icon: Icon(Icons.mic, color: Colors.blueGrey)
//             )
//           )
//         ],
//       ),
//     ),
//   );
// }


import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/utils/helper.dart';
import 'package:flutter_tts/flutter_tts.dart';

class Body extends StatelessWidget {
  final int index;
  Body({this.index});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
      future: FirebaseFirestore.instance.collection("types").doc("P52EgkVrppSyqm2ISTGM").collection("malaise").get(),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              expandedHeight: 200,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(snapshot.data.docs[index]['title'], /*style: Helper.getTheme(context).headline3,*/),
                background: Container(
                  height: 250,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        Colors.black.withOpacity(0.7),
                        Colors.black.withOpacity(0.2),
                      ],
                    ),
                  ),
                  child: Image.asset(Helper.getAssetName("dessert.jpg"), fit: BoxFit.cover),
                ),
              ),
            ),
            _buildWidget(snapshot, index)
          ],
        );
      },
    );
  }

  Future _speak(AsyncSnapshot snapshot, int i) async {
    FlutterTts flutterTts = FlutterTts();
    await flutterTts.setLanguage("fr-FR");
    await flutterTts.setPitch(1);
    print(await flutterTts.getVoices);
    await flutterTts.speak("description. \n "+snapshot.data.docs[i]['desc']+ "Traitements.\n "+snapshot.data.docs[i]['traitement']);
  }
  
  Widget _buildWidget(AsyncSnapshot snapshot, int i) => SliverToBoxAdapter(
    child: ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: 1,
      // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context, index) => Column(
        children: [
          DescriptionOrTraitementContainer(
            i: this.index,
            snapshot: snapshot,
            title: "DESCRIPTION",
            titleDoc: 'desc',
          ),
          DescriptionOrTraitementContainer(
            i: this.index,
            snapshot: snapshot,
            title: "TRAITEMENTS",
            titleDoc: 'traitement',
          ),
          Container(
            height: Helper.getScreenHeight(context) /2,
            width: double.infinity,
            child: Column(
              children: [
                Expanded(flex: 1, child: Text("Voir en images", style: Helper.getTheme(context).headline5)),
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                    child: Swiper(
                      itemBuilder: (BuildContext context, int index) {
                        return Image.asset(
                          Helper.getAssetName("dessert.jpg"),
                          fit: BoxFit.cover,
                        );
                      },
                      itemCount: 10,
                      itemWidth: 300.0,
                      layout: SwiperLayout.STACK,
                    ),
                  ),
                ),
              ],
            ),
          ),
                SizedBox(height: 100),
        ],
      ),
    ),
  );
}

class DescriptionOrTraitementContainer extends StatelessWidget {
  const DescriptionOrTraitementContainer({
    @required this.snapshot,
    @required this.i,
    @required this.title,
    @required this.titleDoc,
    Key key,
  }) : super(key: key);

  final AsyncSnapshot snapshot;
  final int i;
  final String title;
  final String titleDoc;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      width: double.infinity,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: placeholder,
            offset: Offset(0,5),
            blurRadius: 10
          )
        ]
      ),
      child: Column(
        children: [
          Center(
            child: Container(
              width: Helper.getScreenWidth(context) / 2,
              height: 50,
              color: Colors.yellow,
              child: Center(child: Text(title, style: Helper.getTheme(context).headline6))
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Text(
              snapshot.data.docs[i][titleDoc],
              style: Helper.getTheme(context).headline5,
              textAlign: TextAlign.justify,
              ),
          ),
        ]
      )
    );
  }
}

class ButtonCallOrRead extends StatelessWidget {
  const ButtonCallOrRead({
    @required this.icons,
    Key key,
  }) : super(key: key);

  final IconData icons;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(color: mPrimaryColor, width: 1),
        boxShadow: [
          BoxShadow(
            color: mPrimaryColor,
          )
        ]
      ),
      child: Icon(icons, color: Colors.white, size: 25),
    );
  }
}
