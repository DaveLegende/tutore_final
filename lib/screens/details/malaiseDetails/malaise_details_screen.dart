import 'package:flutter/material.dart';
import 'components/body.dart';

class MalaiseDetailScreen extends StatelessWidget {
  final int index;
  MalaiseDetailScreen({this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(index: index),
    );
  }
}