import 'package:flutter/material.dart';
import 'components/body.dart';

class AccidentDetailScreen extends StatelessWidget {
  final int index;
  AccidentDetailScreen({this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(index: index),
    );
  }
}
