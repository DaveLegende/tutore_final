import 'package:flutter/material.dart';
import 'components/body.dart';

class BrulureDetailScreen extends StatelessWidget {
  final int index;
  BrulureDetailScreen({this.index});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(index: index),
    );
  }
}
