import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/utils/helper.dart';
import 'package:flutter_tts/flutter_tts.dart';

class Body extends StatelessWidget {

  final int index;
  Body({this.index});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
      future: FirebaseFirestore.instance.collection("types").doc("9O7KaV9GNBN5gX1eiQhX").collection("brulure").get(),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        return CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              expandedHeight: 200,
              flexibleSpace: FlexibleSpaceBar(
                centerTitle: true,
                title: Text(snapshot.data.docs[index]['title'], /*style: Helper.getTheme(context).headline3,*/),
                background: Container(
                  height: 250,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                        Colors.black.withOpacity(0.7),
                        Colors.black.withOpacity(0.2),
                      ],
                    ),
                  ),
                  child: Image.asset(Helper.getAssetName("dessert.jpg"), fit: BoxFit.cover),
                ),
              ),
            ),
            _buildWidget(snapshot, index)
          ],
        );
      },
    );
  }

  Future _speak(AsyncSnapshot snapshot, int i) async {
    
    FlutterTts flutterTts = FlutterTts();
    await flutterTts.setLanguage("fr-FR");
    await flutterTts.setPitch(1);
    print(await flutterTts.getVoices);
    await flutterTts.speak(snapshot.data.docs[i]['desc']);
  }
  Widget _buildWidget(AsyncSnapshot snapshot, int i) => SliverToBoxAdapter(
    child: ListView.builder(
      primary: false,
      shrinkWrap: true,
      itemCount: 1,
      // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (context, index) => Stack(
        children: [
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: placeholder,
                  offset: Offset(0,5),
                  blurRadius: 10
                )
              ]
            ),
            child: Column(
              children: [
                Container(
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          width: Helper.getScreenWidth(context) / 2,
                          height: 50,
                          color: Colors.yellow,
                          child: Center(child: Text("DESCRIPTION", style: Helper.getTheme(context).headline6))
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Text(
                          snapshot.data.docs[i]['desc'],
                          style: Helper.getTheme(context).headline5,
                          textAlign: TextAlign.justify,
                          ),
                      ),
                    ],
                  ),
                ),
                Container(
                  child: Column(
                    children: [
                      Center(
                        child: Container(
                          width: Helper.getScreenWidth(context) / 2,
                          height: 50,
                          color: Colors.yellow,
                          child: Center(child: Text("TRAITEMENTS", style: Helper.getTheme(context).headline6))
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Text(
                          snapshot.data.docs[i]['traitement'],
                          style: Helper.getTheme(context).headline5,
                          textAlign: TextAlign.justify,
                          ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 100),
              ],
            ),
          ),
          Positioned(
            bottom: 0,
            right: 20,
            child: IconButton(
              color: Colors.yellow,
              onPressed: () {
                _speak(snapshot, i);
              },
              icon: Icon(Icons.mic, color: Colors.blueGrey)
            )
          )
        ],
      ),
    ),
  );
}
