import 'package:flutter/material.dart';
import 'package:tutore/utils/constants.dart';

enum MenuOption { Langues, Aides, FAQ, Exit }

class PopOptionMenu extends StatelessWidget {
  const PopOptionMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<MenuOption>(
        itemBuilder: (context) => <PopupMenuEntry<MenuOption>>[
              PopupMenuItem(
                child: Text("Langues"),
                value: MenuOption.Langues,
              ),
              PopupMenuItem(
                child: Text("Aides"),
                value: MenuOption.Aides,
              ),
              PopupMenuItem(
                child: Text("FAQ"),
                value: MenuOption.FAQ,
              ),
              PopupMenuItem(
                child: Text("Exit"),
                value: MenuOption.Exit,
              ),
            ],
        child: IconButton(
          icon: Icon(Icons.more_vert, color: mPrimaryColor),
          onPressed: null,
        ));
  }
}
