import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/models/category_model.dart';
import 'package:tutore/screens/types/plaie/type_screen.dart';
import 'package:tutore/screens/types/accident/type_accident_screen.dart';
import 'package:tutore/screens/types/brulure/type_brulure_screen.dart';
import 'package:tutore/screens/types/malaise/type_malaise_screen.dart';
import 'package:tutore/utils/helper.dart';
import 'package:tutore/utils/constants.dart';

class Body extends StatefulWidget {
  const Body({
    Key key,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

CategoryModel brulure;
CategoryModel plaie;
CategoryModel malaise;
CategoryModel accident;

class _BodyState extends State<Body> {

  @override
  void initState() {
    // getData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
    //   future: FirebaseFirestore.instance.collection("category").get(),
    //   builder: (context, snapshot) {
    //     if(snapshot.connectionState == ConnectionState.waiting) {
    //       return Center(child: CircularProgressIndicator());
    //     }
    //     brulure = CategoryModel(image: snapshot.data.docs[0]["image"], title: snapshot.data.docs[0]["title"]);
    //     plaie = CategoryModel(image: snapshot.data.docs[1]["image"], title: snapshot.data.docs[1]["title"]);
    //     malaise = CategoryModel(image: snapshot.data.docs[2]["image"], title: snapshot.data.docs[2]["title"]);
    //     accident = CategoryModel(image: snapshot.data.docs[3]["image"], title: snapshot.data.docs[3]["title"]);
    //     print(brulure.title + accident.title + malaise.title + plaie.title);
    //     return Center(
    //       child: GridView.builder(
    //         gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    //         itemCount: snapshot.data.docs.length,
    //         itemBuilder: (context, i) {
    //           return Container(
    //             // margin: EdgeInsets.all(2.5),
    //             child: Card(
    //               elevation: 10.0,
    //               child: Container(
    //                 height: 200,
    //                 width: double.infinity,
    //                 child: InkWell(
    //                     onTap: () {
    //                       Navigator.push(context, MaterialPageRoute(
    //                         builder: (context) => TypePlaieScreen(
    //                           future: FirebaseFirestore.instance.collection("types").doc("UYz5wG3jrSwA5fHS5RiV").collection("plaie").get(),
    //                         ),
    //                       ));
    //                     },
    //                   child: Column(
    //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //                     children: [
    //                       Container(
    //                         height: 150,
    //                         child: ClipRRect(
    //                           borderRadius: BorderRadius.circular(10),
    //                           child: Image.network(
    //                             snapshot.data.docs[i]["image"], 
    //                             fit: BoxFit.cover
    //                           )
    //                         ),
    //                       ),
    //                       Container(
    //                         height: 20,
    //                         child: Text(
    //                           snapshot.data.docs[i]["title"], 
    //                           style: Helper.getTheme(context).headline5
    //                         ),
    //                       )
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ),
    //           );
    //         },
    //       ),
    //     );
    //   },
    // );
    return FutureBuilder(
      future: FirebaseFirestore.instance.collection("category").get(),
      builder: (context, snapshot) {
        // if(snapshot.connectionState == ConnectionState.waiting){
        //   return Center(child: CircularProgressIndicator());
        // }
        if(snapshot.data == null) {
          return Center(child: CircularProgressIndicator());
        }
        return Container(
          width: Helper.getScreenWidth(context),
          height: Helper.getScreenHeight(context),
          child: Column(
            children: [
              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CategoryCard(
                          index: 0,
                          snapshot: snapshot,
                          object: TypePlaieScreen(
                            future: FirebaseFirestore.instance.collection("types").doc("0VLIUGXCcILh91rmJNW0").collection("accident").get(),
                          ),
                        )
                      ),
                      Expanded(
                        child: CategoryCard(
                          index: 1,
                          snapshot: snapshot,
                          object: TypePlaieScreen(
                            future: FirebaseFirestore.instance.collection("types").doc("9O7KaV9GNBN5gX1eiQhX").collection("brulure").get(),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ),

              Expanded(
                flex: 1,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: [
                      Expanded(
                        child: CategoryCard(
                          index: 2,
                          snapshot: snapshot,
                          object: TypePlaieScreen(
                            future: FirebaseFirestore.instance.collection("types").doc("P52EgkVrppSyqm2ISTGM").collection("malaise").get(),
                          ),
                        ),
                      ),
                      Expanded(
                        child: CategoryCard(
                          index: 3,
                          snapshot: snapshot,
                          object: TypePlaieScreen(
                            future: FirebaseFirestore.instance.collection("types").doc("UYz5wG3jrSwA5fHS5RiV").collection("plaie").get(),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ),
            ]
          ),
        );
      }
    );
  }

  // Future<DocumentSnapshot> getData() async {
  //   await Firebase.initializeApp();
  //   return await FirebaseFirestore.instance.collection("category").get();
  // }
}

class CategoryCard extends StatelessWidget {
  const CategoryCard({
    @required int index,
    @required AsyncSnapshot snapshot,
    @required Object object,
    Key key,
  }) : _index = index, _snapshot = snapshot, _object = object, super(key: key);

  final int _index;
  final AsyncSnapshot _snapshot;
  final Object _object;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: GestureDetector(
        onTap: (){
          Navigator.push(context, MaterialPageRoute(
            builder: (context) => _object,
          ));
        },
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: placeholder,
                offset: Offset(0,5),
                blurRadius: 10
              )
            ]
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                height: 150,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Image.network(
                    _snapshot.data.docs[_index]["image"], 
                    fit: BoxFit.cover
                  )
                ),
              ),
              Container(
                height: 20,
                child: Text(
                  _snapshot.data.docs[_index]["title"], 
                  style: Helper.getTheme(context).headline5
                ),
              )
            ],
          )
        ),
      )
    );
  }
}