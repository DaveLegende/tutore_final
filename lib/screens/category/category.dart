import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tutore/screens/addmalaise/add.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/utils/helper.dart';
import '../forum/forum.dart';
import '../profile/profile.dart';

import 'components/appBar.dart';
import 'components/body.dart';

class Category extends StatefulWidget {
  static const String routeName = '/categoryScreen';
  const Category({Key key}) : super(key: key);

  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {

  int _currentIndex;

  @override
  void initState() { 
    _currentIndex = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    List<Widget> _itemSelected = [
      Body(),
      Forum(),
      Profile()
    ];
    return Scaffold(
      appBar: _appBar("Secours"),
      bottomNavigationBar: _bottomNav(),
      body: _itemSelected[_currentIndex],
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Add()
            ),
          );
        },
        child: Icon(Icons.add, color: Colors.white),
      ),
    );
  }

  BottomNavyBar _bottomNav() {
    return BottomNavyBar(
      selectedIndex: _currentIndex,
      onItemSelected: (index) {
        setState(() {
          _currentIndex = index;
        });
      },
      items: [
        BottomNavyBarItem(
          icon: Icon(Icons.home),
          title: Text("Home"),
          activeColor: mPrimaryColor,
          inactiveColor: mcardbtnColor,
        ),
        BottomNavyBarItem(
          icon: Icon(MaterialCommunityIcons.account_group),
          title: Text("Forum"),
          activeColor: mPrimaryColor,
          inactiveColor: mcardbtnColor,
        ),
        BottomNavyBarItem(
          icon: Icon(FontAwesome.user_circle),
          title: Text("Profile"),
          activeColor: mPrimaryColor,
          inactiveColor: mcardbtnColor,
        ),
      ],
    );
  }

  _appBar(String title) {
    return AppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      title: Text(title, style: Helper.getTheme(context).headline6),
      actions: [
        PopOptionMenu()
      ],
    );
  }
}
