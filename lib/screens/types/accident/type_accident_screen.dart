import 'package:flutter/material.dart';
import 'package:tutore/utils/helper.dart';
import 'components/body.dart';


class TypeAccidentScreen extends StatefulWidget {
  const TypeAccidentScreen({ Key key }) : super(key: key);

  @override
  _TypeAccidentScreenState createState() => _TypeAccidentScreenState();
}

class _TypeAccidentScreenState extends State<TypeAccidentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text("Accidents", style: Helper.getTheme(context).headline6),
      ),
      body: Body()
    );
  }
}