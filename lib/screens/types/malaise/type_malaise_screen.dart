import 'package:flutter/material.dart';
import 'components/body.dart';
import 'package:tutore/utils/helper.dart';

class TypeMalaiseScreen extends StatefulWidget {
  const TypeMalaiseScreen({ Key key }) : super(key: key);

  @override
  _TypeMalaiseScreenState createState() => _TypeMalaiseScreenState();
}

class _TypeMalaiseScreenState extends State<TypeMalaiseScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar(context, "Malaise"),
      body: Body()
    );
  }

  AppBar _appBar(BuildContext context, String title) {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      title: Text(title, style: Helper.getTheme(context).headline6)
    );
  }
}