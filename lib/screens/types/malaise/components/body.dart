import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/components/custom_triangle.dart';
import '../../../../components/type_card_list.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/screens/details/details_screens.dart';
import 'package:tutore/utils/helper.dart';
// import 'package:tutore/models/type_model.dart';

class Body extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    // TypeModel simple;
    // TypeModel saignement;
    // TypeModel oeil;
    // TypeModel section;
    // return CustomFutureBuilder(collectionTitle: "malaise", docTitle: "P52EgkVrppSyqm2ISTGM", object: MalaiseDetailScreen(index: _i));
    return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
      future: FirebaseFirestore.instance.collection("types").doc("P52EgkVrppSyqm2ISTGM").collection("malaise").get(),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        // simple = TypeModel(title: snapshot.data.docs[0]['title'], desc: snapshot.data.docs[0]['desc'], image: snapshot.data.docs[0]['image']);
        // saignement = TypeModel(title: snapshot.data.docs[1]['title'], desc: snapshot.data.docs[1]['desc'], image: snapshot.data.docs[1]['image']);
        // oeil = TypeModel(title: snapshot.data.docs[2]['title'], desc: snapshot.data.docs[2]['desc'], image: snapshot.data.docs[2]['image']);
        // section = TypeModel(title: snapshot.data.docs[3]['title'], desc: snapshot.data.docs[3]['desc'], image: snapshot.data.docs[3]['image']);
        return Container(
          height: Helper.getScreenHeight(context),
          width: Helper.getScreenWidth(context),
          child: Stack(
            children: [
              Container(
                width: 100,
                height: Helper.getScreenHeight(context),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(100), bottomRight: Radius.circular(50)),
                  color: mPrimaryColor
                ),
              ),
              ListView.builder(
                itemCount: snapshot.data.docs.length,
                itemBuilder: (context, i) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (context) => DetailScreen())
                        );
                      },
                      child: TypeListCard(
                        title: snapshot.data.docs[i]['title'],
                        desc: snapshot.data.docs[i]['desc'],
                        imageShape: ClipPath(
                          clipper: CustomTriangle(),
                          child: Container(
                            width: 80,
                            height: 80,
                            child: Image.asset(Helper.getAssetName("dessert.jpg"), fit: BoxFit.cover),
                          ),
                        )
                      )
                    )
                  );
                },
              )
            ],
          ),
        );
      },
    );
  }
}