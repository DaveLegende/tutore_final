import 'package:flutter/material.dart';
import 'package:tutore/utils/helper.dart';
import 'components/body.dart';

class TypeBrulureScreen extends StatefulWidget {
  const TypeBrulureScreen({ Key key }) : super(key: key);

  @override
  _TypeBrulureScreenState createState() => _TypeBrulureScreenState();
}

class _TypeBrulureScreenState extends State<TypeBrulureScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text("Brulure", style: Helper.getTheme(context).headline6),
      ),
      body: Body()
    );
  }
}