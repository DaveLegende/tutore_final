import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/screens/types/plaie/components/body.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/utils/helper.dart';

class TypePlaieScreen extends StatefulWidget {
  final Future<QuerySnapshot<Map<String, dynamic>>> future;
  static const routeName = "/typeScreen";
  const TypePlaieScreen({ @required this.future, Key key }) : super(key: key);

  @override
  _TypeScreenPlaieState createState() => _TypeScreenPlaieState();
}

class _TypeScreenPlaieState extends State<TypePlaieScreen> {
  bool isSearching = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 10,
        backgroundColor: Colors.white,
        title: !isSearching 
          ? Text("Plaie", style: Helper.getTheme(context).headline6) 
          : TextField(decoration: InputDecoration(hintText: "Search...")),
        actions: [
          isSearching
          ? IconButton(
            onPressed: () {
              setState(() {
                isSearching = !isSearching;
              });
            },
            icon: Icon(Icons.cancel, color: mPrimaryColor),
          )
          : IconButton(
            onPressed: () {
              setState(() {
                isSearching = !isSearching;
              });
            },
            icon: Icon(Icons.search, color: mPrimaryColor),
          )
        ],
        iconTheme: IconThemeData(color: mPrimaryColor),
      ),
      body: Body(future: widget.future),
    );
  }
}