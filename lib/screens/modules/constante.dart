import 'package:flutter/material.dart';

const kTextField = InputDecoration(
  prefixIcon: Icon(
    Icons.person,
  ),
  //  ,
  //  icon:

  hintText: 'Entrer votre nom d\'utilisateur',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(20.0)),
    borderSide: BorderSide(
      color: Colors.redAccent,
    ),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(25.0)),
    borderSide: BorderSide(color: Colors.redAccent, width: 1.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
    borderSide: BorderSide(color: Colors.redAccent, width: 2.0),
  ),
);
const kOffreTextField = InputDecoration(
  hintText: 'Entrer votre nom d\'utilisateur',
  contentPadding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
  border: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(30.0)),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(15.0)),
    borderSide: BorderSide(color: Colors.redAccent, width: 1.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(32.0)),
    borderSide: BorderSide(color: Colors.redAccent, width: 2.0),
  ),
);

const kOffreTextStyle =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 15.0);
