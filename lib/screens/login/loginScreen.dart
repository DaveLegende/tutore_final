// import 'package:flutter/material.dart';
// import 'package:tutore/components/customTextInput.dart';
// import 'package:tutore/utils/helper.dart';

// class LoginScreen extends StatelessWidget {
//   static const routeName = "/loginScreen";
//   const LoginScreen({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: SingleChildScrollView(
//       child: Container(
//         width: Helper.getScreenWidth(context),
//         height: Helper.getScreenHeight(context),
//         child: SafeArea(
//           child: Form(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
//               child: Column(children: [
//                 Text(
//                   "Login",
//                   style: Helper.getTheme(context).headline6,
//                 ),
//                 Spacer(),
//                 CustomTextInput(hintText: "Your email"),
//                 Spacer(),
//                 CustomTextInput(hintText: "Your password"),
//                 Spacer(),
//                 SizedBox(
//                     width: double.infinity,
//                     height: 50,
//                     child: ElevatedButton(
//                         style: ButtonStyle(
//                             shape: MaterialStateProperty.all(StadiumBorder())),
//                         onPressed: () {},
//                         child: Text("Login"))),
//                 Spacer(),
//                 GestureDetector(
//                   child: Text("Forget your password?"),
//                   onTap: () {
//                     // Navigator.of(context).pushReplacementNamed(ForgetPwScreen.routeName);
//                   },
//                 ),
//                 Spacer(flex: 2),
//                 Text("Or login with"),
//                 Spacer(),
//                 SizedBox(
//                   width: double.infinity,
//                   height: 50,
//                   child: ElevatedButton(
//                       style: ButtonStyle(
//                           shape: MaterialStateProperty.all(StadiumBorder()),
//                           backgroundColor:
//                               MaterialStateProperty.all(Color(0xFF367FC0))),
//                       onPressed: () {},
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Image.asset(Helper.getAssetName("fb.png")),
//                           SizedBox(width: 30),
//                           Text("Login with Facebook")
//                         ],
//                       )),
//                 ),
//                 Spacer(),
//                 SizedBox(
//                   width: double.infinity,
//                   height: 50,
//                   child: ElevatedButton(
//                       style: ButtonStyle(
//                           shape: MaterialStateProperty.all(StadiumBorder()),
//                           backgroundColor:
//                               MaterialStateProperty.all(Color(0xFFDD4B39))),
//                       onPressed: () {},
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         children: [
//                           Image.asset(Helper.getAssetName("google.png")),
//                           SizedBox(width: 30),
//                           Text("Login with Google")
//                         ],
//                       )),
//                 ),
//               ]),
//             ),
//           ),
//         ),
//       ),
//     ));
//   }
// }


import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutore/screens/modules/constante.dart';

class ConnexionScreen extends StatefulWidget {
  static const routeName = "/loginScreen";
  ConnexionScreen();

  @override
  _ConnexionScreenState createState() => _ConnexionScreenState();
}

final _scaffoldkey = GlobalKey<ScaffoldState>();

class _ConnexionScreenState extends State<ConnexionScreen> {
  final _formKey = GlobalKey<FormState>();
  FirebaseAuth _auth = FirebaseAuth.instance;
  bool showMe = false;
  String mail;
  String password;
  bool showspiner = false;
  String errorMessage = 'kf';
  bool _autoValidate = false;
  bool _showReset = false;
  void _validateInputs() {
    if (_formKey.currentState.validate()) {
// Si toutes les données sont correctes, enregistrez les données dans les variables

      _formKey.currentState.save();
    } else {
// Si toutes les données ne sont pas valides, lancez la validation automatique.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // if ( showMe) {
    //  snack('Un email est envoyé a $mail.Suivez la procédure de rénitialisation qui y est indiquée');

    // }
    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Premiers Soins',
          style: TextStyle(fontFamily: 'LobsterTwo'),
        ),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: Center(
          child: Form(
            key: _formKey,
            child: ListView(children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.25,
                child: Hero(tag: 'logo', child: Image.asset('images/logo.png')),
              ),
              Text(
                'Se Connecter',
                style: TextStyle(
                  fontFamily: 'Rokkitt',
                  fontSize: MediaQuery.of(context).size.height * 0.03,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 18.0,
              ),
              TextFormField(
                textAlign: TextAlign.center,
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) {
                  mail = value;
                },
                decoration: kTextField.copyWith(
                  hintText: 'Entrer votre E-mail',
                  prefixIcon: Icon(
                    Icons.mail,
                  ),
                ),
                validator: (value) {
                  if (value.length < 3)
                    return 'Name must be more than 2 charater';
                  else
                    return null;
                },
                autovalidate: _autoValidate,
              ),
              SizedBox(height: 15.0),
              TextFormField(
                textAlign: TextAlign.center,
                obscureText: true,
                keyboardType: TextInputType.text,
                onChanged: (value) {
                  password = value;
                },
                decoration: kTextField.copyWith(
                  hintText: 'Entrer votre mot de passe',
                  prefixIcon: Icon(
                    Icons.lock,
                  ),
                ),
                validator: (value) => value.length < 6
                    ? 'Mot de passe doit contenir au moins 6 cractères'
                    : null,
                autovalidate: _autoValidate,
              ),
              SizedBox(height: 15.0),
              Material(
                  // borderRadius:  BorderRadius.all(Radius.circular(32.0)),
                  child: MaterialButton(
                      child: Text(
                        'Se connecter',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.height * 0.03,
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(22.0)),
                      ),
                      color: Colors.blueAccent,
                      onPressed: () async {
                        // setState(() {
                        //   showspiner=true;
                        //     });i
                        if (_formKey.currentState.validate()) {
                          _validateInputs();

                          try {
                            print(mail);
                            print(password);
                            final result = await FirebaseAuth.instance
                                .signInWithEmailAndPassword(
                                    email: mail, password: password);
                            if (result != null) {
                              SharedPreferences prefs =
                                  await SharedPreferences.getInstance();
                              prefs.setString('email', mail);
                              Navigator.pushReplacementNamed(
                                  context, '/profile');
                            }
                          } catch (error) {
                            if (error.toString() ==
                                '[firebase_auth/wrong-password] The password is invalid or the user does not have a password.') {
                              setState(() {
                                _showReset = true;
                              });
                            }
                            setState(() {
                              switch (error.toString()) {
                                case '[firebase_auth/wrong-password] The password is invalid or the user does not have a password.':
                                  errorMessage = 'Mot de passe invalide ';
                                  break;
                                case '[firebase_auth/invalid-email] The email address is badly formatted.':
                                  errorMessage =
                                      'L\'addresse email est mal formaté';
                                  break;
                                case '[firebase_auth/user-not-found] There is no user record corresponding to this identifier. The user may have been deleted.':
                                  errorMessage =
                                      'Il n\'y a pas d\'enregistrement d\'utilisateur correspondant à cet identifiant';

                                  break;
                                case '[firebase_auth/too-many-requests] We have blocked all requests from this device due to unusual activity. Try again later.':
                                  errorMessage =
                                      'Trop d\'essai sur cet appareil réessayez plus tard';
                                  break;
                                default:
                                  errorMessage = error.toString();
                              }
                            });
                            Alert(
                              context: context,
                              type: AlertType.error,
                              title: errorMessage,
                              //desc: "Flutter is more awesome with RFlutter Alert.",
                              buttons: [
                                DialogButton(
                                  child: Text(
                                    'reesayez',
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20),
                                  ),
                                  onPressed: () {
                                    var nav = Navigator.of(context);
                                    nav.pop();
                                  },
                                  width: 120,
                                )
                              ],
                            ).show();
                          }
                        }
                      })),
              SizedBox(
                height: 15.0,
              ),
              Center(
                child: Column(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // _showReset

                    Row(
                      children: [
                        Text(
                          'Vous êtes nouveau?',
                        ),
                        FlatButton(
                          color: Colors.white,
                          child: Text(
                            'S\'inscrire',
                            style: TextStyle(
                                color: Colors.redAccent,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            setState(() {
                              showspiner = true;
                            });
                            Navigator.pushReplacementNamed(context, '/');
                            setState(() {
                              showspiner = false;
                            });
                          },
                        )
                      ],
                    ),
                    Row(
                      children: [
                        Text('Mot de passe oublié ?'),
                        FlatButton(
                          child: Text(
                            'Cliquez ici',
                            style: TextStyle(
                                color: Colors.redAccent,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            Navigator.pushNamed(context, '/resetP');
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              )
            ]),
          ),
        ),
      ),
    );
  }
}
// void snack(String title){
//   final  snackbar=  SnackBar(
//                                   content: Text(title),
//                                   backgroundColor: Colors.red,
//                                 );
//                                 _scaffoldkey.currentState.showSnackBar(snackbar);
//                         }
