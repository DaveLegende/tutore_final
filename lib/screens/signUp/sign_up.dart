// import 'package:flutter/material.dart';
// import 'package:tutore/screens/login/loginScreen.dart';
// import 'package:tutore/services/auth.dart';
// // import 'package:tutore/services/auth.dart';
// import 'package:tutore/utils/constants.dart';
// import 'package:tutore/utils/helper.dart';

// class SignUpScreen extends StatefulWidget {
//   static const routeName = "/signUpScreen";

//   @override
//   _SignUpScreenState createState() => _SignUpScreenState();
// }

// class _SignUpScreenState extends State<SignUpScreen> {

//   AuthServices auth = AuthServices();
//   String username ='';
//   String email ='';
//   String password ='';
//   String confirmPassword ='';
//   final _formKey = GlobalKey<FormState>();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: SingleChildScrollView(
//         child: Container(
//           width: Helper.getScreenWidth(context),
//           height: Helper.getScreenHeight(context),
//           child: SafeArea(
//             child: Container(
//               padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//               child: Form(
//                 key: _formKey,
//                 child: Column(
//                   children: [
//                     Text(
//                       "Créer un Compte",
//                       style: Helper.getTheme(context).headline4,
//                     ),
//                     Spacer(),
//                     TextFormField(
//                       decoration: InputDecoration(
//                         hintText: "Username",
//                         hintStyle: TextStyle(color: placeholder),
//                         border: InputBorder.none,
//                         contentPadding: const EdgeInsets.only(left: 40),
//                       ),
//                       validator: (val) => val.isEmpty ? mNamelNullError : null,
//                       onChanged: (val) => username = val,
//                     ),
//                     Spacer(),
//                     TextFormField(
//                       decoration: InputDecoration(
//                         hintText: "Email",
//                         hintStyle: TextStyle(color: placeholder),
//                         border: InputBorder.none,
//                         contentPadding: const EdgeInsets.only(left: 40)
//                       ),
//                       validator: (val) => val.isEmpty ? mInvalidEmailError : null,
//                       onChanged: (val) => username = val,
//                     ),
//                     Spacer(),
//                     TextFormField(
//                       decoration: InputDecoration(
//                         hintText: "Password",
//                         hintStyle: TextStyle(color: placeholder),
//                         border: InputBorder.none,
//                         contentPadding: const EdgeInsets.only(left: 40)
//                       ),
//                       validator: (val) => val.length < 6 ? mShortPassError : null,
//                       onChanged: (val) => password = val,
//                       obscureText: true,
//                     ),
//                     Spacer(),
//                     TextFormField(
//                       decoration: InputDecoration(
//                         hintText: "Confirm Password",
//                         hintStyle: TextStyle(color: placeholder),
//                         border: InputBorder.none,
//                         contentPadding: const EdgeInsets.only(left: 40)
//                       ),
//                       validator: (val) => confirmPassword != password ? mMatchPassError : null,
//                       onChanged: (val) => confirmPassword = val,
//                       obscureText: true,
//                     ),
//                     Spacer(),
//                     SizedBox(
//                       width: double.infinity,
//                       height: 50,
//                       child: ElevatedButton(
//                         style: ButtonStyle(
//                           shape: MaterialStateProperty.all(StadiumBorder()),
//                           backgroundColor: MaterialStateProperty.all(mPrimaryColor)
//                         ),
//                         onPressed: () async{
//                           if(_formKey.currentState.validate()){
//                             print(email +"  "+ password);
//                             bool register = await auth.signUp(email, password);
//                             if(register) {
//                               Navigator.push(context, 
//                                 MaterialPageRoute(builder: (context) => Container(color: mPrimaryColor))
//                               );
//                             }
//                           }
//                         }, 
//                         child: Text("Sign Up")
//                       )
//                     ),
//                     Spacer(),
//                     SizedBox(
//                       width: double.infinity,
//                       height: 50,
//                       child: ElevatedButton(
//                         style: ButtonStyle(
//                           shape: MaterialStateProperty.all(StadiumBorder())
//                         ),
//                         onPressed: (){
//                           Navigator.of(context).pushReplacementNamed(ConnexionScreen.routeName);
//                         }, 
//                         child: Text("Login")
//                       )
//                     ),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }


//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutore/screens/profile/profile.dart';

import '../modules/constante.dart';

class InscriptionScreen extends StatefulWidget {
  static const routeName = "/signUpScreen";
  @override
  _InscriptionScreenState createState() => _InscriptionScreenState();
}

class _InscriptionScreenState extends State<InscriptionScreen> {
  bool fonecheck = false;
  bool existant = false;

  final _auth = FirebaseAuth.instance;
  User util;
  final _scaffoldkey = GlobalKey<ScaffoldState>();
  String confrmPass = '';
//collection utilisateur

  final CollectionReference collectionUser =
      FirebaseFirestore.instance.collection('utilisateurs');
  final _formKey = GlobalKey<FormState>();
  TextEditingController _password = TextEditingController();
  TextEditingController _passwordConfirm = TextEditingController();

  bool chargement = false;
  String mail = '';
  bool admin = false;
  bool checkMail = false;
  String password = '';
  String username = '';
  String errorMessage = '';
  bool _autoValidate = false;
  String warning = '';
  void _validateInputs() {
    if (_formKey.currentState.validate()) {
// Si toutes les données sont correctes, enregistrez les données dans les variables
      _formKey.currentState.save();
    } else {
// Si toutes les données ne sont pas valides, lancez la validation automatique.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // validatePhone();

    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Premiers Soins',
          style: TextStyle(
            fontFamily: 'LobsterTwo',
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.redAccent,
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24.0),
          child: Container(
            child: Form(
              key: _formKey,
              child: ListView(
                  // mainAxisAlignment: MainAxisAlignment.center,
                  // crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      child: Hero(
                          tag: 'logo', child: Image.asset('images/logo.png')),
                    ),

                    Text(
                      'Premiers Soins',
                      style: TextStyle(fontFamily: 'Rokkitt'),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 18.0,
                    ),

                    TextFormField(
                      textAlign: TextAlign.center,
                      keyboardType: TextInputType.emailAddress,
                      onChanged: (value) {
                        mail = value;
                      },
                      decoration: kTextField.copyWith(
                        hintText: ' E-Mail',
                        prefixIcon: Icon(
                          Icons.mail,
                        ),
                      ),
                      validator: (value) {
                        var pattern =
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                        RegExp regex = new RegExp(pattern);
                        if (!regex.hasMatch(value))
                          return 'Entrer un Email valide';
                        else
                          return null;
                      },
                      autovalidate: _autoValidate,
                    ),
                    SizedBox(height: 15.0),
                    TextFormField(
                      controller: _password,
                      textAlign: TextAlign.center,
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      decoration: kTextField.copyWith(
                        hintText: 'Mot de passe',
                        prefixIcon: Icon(
                          Icons.lock,
                        ),
                      ),
                      validator: (value) {
                        confrmPass = value;
                        if (value.length < 6) {
                          return 'Mot de passe doit contenir au moins 6 cractères';
                        }
                        return null;
                      },
                      autovalidate: _autoValidate,
                    ),
                    SizedBox(height: 15.0),
                    TextFormField(
                      controller: _passwordConfirm,
                      textAlign: TextAlign.center,
                      obscureText: true,
                      keyboardType: TextInputType.text,
                      onChanged: (value) {
                        password = value;
                      },
                      decoration: kTextField.copyWith(
                        hintText: 'Confirmer le mot de passe',
                        prefixIcon: Icon(
                          Icons.lock,
                        ),
                      ),
                      validator: (val) {
                        if (val.isEmpty) {
                          return 'Ce champ ne peut pass être vide';
                        } else if (val != _password.text) {
                          return 'Le mot de passe ne correspond pas ';
                        }
                        return null;
                      },
                      autovalidate: _autoValidate,
                    ),

                    //decoration: kTextField.copyWith(hintText:'+228 00000000', prefixIcon: Icon( Icons.call ), ),
                    //  validator: (value)=>value.length<12 ? 'Entrer un numero de téléphone': null,
                    // controller: _phoneController,
                    SizedBox(height: 15.0),
                    Material(
                      //borderRadius:  BorderRadius.all(Radius.circular(32.0)),
                      child: MaterialButton(
                          child: Text(
                            'S\'inscrire',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize:
                                  MediaQuery.of(context).size.height * 0.03,
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(22.0)),
                          ),
                          color: Colors.blueAccent,
                          onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              //  final mobile=_phoneController.text.trim();

                              // phoneverication(phone);
                              try {
                                final user =
                                    await _auth.createUserWithEmailAndPassword(
                                        email: mail, password: password);
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                prefs.setString('email', mail);
                                await FirebaseFirestore.instance
                                    .collection('info')
                                    .add({
                                  'user_id':
                                      FirebaseAuth.instance.currentUser.uid,
                                  'nom': '',
                                  'prenom': '',
                                  'Profession': '',
                                  'residence': '',
                                  'age': '',
                                  'groupe_sanguin': '',
                                  'email': mail,
                                  'numero1': '',
                                  'numero2': '',
                                  'about': '',
                                  'imgUrl': '',
                                  'admin': false,
                                });
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Profile()));
                              } catch (error) {
                                print(error);
                                setState(() {
                                  switch (error.toString()) {
                                    case '[firebase_auth/unknown] Given String is empty or null':
                                      errorMessage =
                                          ' L\'adresse e-mail ne peut pas être vide.';
                                      break;
                                    case '[firebase_auth/email-already-in-use] The email address is already in use by another account.':
                                      errorMessage =
                                          ' L\'adresse e-mail est déjà utilisée par un autre compte.';
                                      break;
                                    case '[firebase_auth/invalid-email] The email address is badly formatted.':
                                      errorMessage =
                                          'L\'addresse email est mal formaté';
                                      break;
                                    case '[firebase_auth/weak-password] Password should be at least 6 characters':
                                      errorMessage =
                                          'Le mot de passe doit comporter au moins 6 caractères';

                                      break;
                                    default:
                                      errorMessage =
                                          'Une erreur s\'est produit au cous de l\'enregistrement';
                                  }
                                });
                                Alert(
                                  context: context,
                                  type: AlertType.error,
                                  title: errorMessage,
                                  //desc: "Flutter is more awesome with RFlutter Alert.",
                                  buttons: [
                                    DialogButton(
                                      child: Text(
                                        'reesayez',
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      onPressed: () {
                                        var nav = Navigator.of(context);
                                        nav.pop();
                                      },
                                      width: 120,
                                    )
                                  ],
                                ).show();
                              }
                            }
                          }),
                    ),

                    SizedBox(height: 15.0),
                    Row(
                      children: [
                        Text(
                          'Vous avez deja un compte ?',
                        ),
                        FlatButton(
                          color: Colors.white,
                          child: Text(
                            'Se connecter',
                            style: TextStyle(
                                color: Colors.redAccent,
                                decoration: TextDecoration.underline),
                          ),
                          onPressed: () {
                            Navigator.pushReplacementNamed(
                                context, '/connection');
                          },
                        ),
                      ],
                    )
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}
