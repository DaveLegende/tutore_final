// import 'package:flutter/material.dart';
// import 'package:tutore/components/initialScreen.dart';

// class Profile extends StatefulWidget {
//   const Profile({ Key key }) : super(key: key);

//   @override
//   _ProfileState createState() => _ProfileState();
// }

// class _ProfileState extends State<Profile> {
//   @override
//   Widget build(BuildContext context) {
//     return InitialScreen();
//   }
// }


import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
// import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tutore/components/initialScreen.dart';
import 'package:tutore/screens/login/loginScreen.dart';
import 'package:tutore/screens/signUp/sign_up.dart';

class Profile extends StatefulWidget {
  const Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  FirebaseAuth _auth = FirebaseAuth.instance;
  var id_i;
  String nom = '';
  String prenom = '';
  String profession = '';
  String residence = '';
  String age = '';
  String groupeS = '';
  String email = '';
  String num1 = '';
  String num2 = '';
  String about = '';
  String imgUrl = '';
  User user = FirebaseAuth.instance.currentUser;
  // final picker = ImagePicker();
  File _imageFile;

  Future pickImage() async {
    // final pickedFile = await picker.pickImage(source: ImageSource.gallery);

    // setState(() {
    //   _imageFile = File(pickedFile.path);
    // });
    // uploadImageToFirebase(context);
  }

  Future uploadImageToFirebase(BuildContext context) async {
    String fileName = _imageFile.path.split('/').last;
    Reference firebaseStorageRef =
        FirebaseStorage.instance.ref().child('uploads/$fileName');
    UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile);
    TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() {});
    taskSnapshot.ref.getDownloadURL().then((value) async {
      print("Done: $value");
      await FirebaseFirestore.instance.collection('info').doc(id_i).update({
        'imgUrl': value,
      });
    });
  }

  void getCurrentUser() async {
    try {
      if (user != null) {
        print(user);
        print(user.uid);
        //  await Provider.of<NombreOffre>(context,listen: false).getnomb(user);
        getData();
      } else {
        print('cc');
      }
    } catch (e) {
      print(e);
    }
  }

  void getData() async {
    String u = '${user.uid}';
    FirebaseFirestore _firestore = FirebaseFirestore.instance;
    await for (var datas in _firestore
        .collection('info')
        .where('user_id', isEqualTo: u)
        .snapshots()) {
      for (var data in datas.docs) {
        print(data.data());
        setState(() {});
        id_i = data.id;
        nom = data.data()['nom'] != null ? data.data()['nom'] : '';
        prenom = data.data()['prenom'] != null ? data.data()['prenom'] : '';
        profession =
            data.data()['profession'] != null ? data.data()['profession'] : '';
        residence =
            data.data()['residence'] != null ? data.data()['residence'] : '';
        age = data.data()['age'] != null ? data.data()['age'] : '';
        groupeS = data.data()['groupe_sanguin'] != null
            ? data.data()['groupe_sanguin']
            : '';
        email = data.data()['email'] != null ? data.data()['email'] : '';
        num1 = data.data()['numero1'] != null ? data.data()['numero1'] : '';
        num2 = data.data()['numero2'] != null ? data.data()['numero2'] : '';
        about = data.data()['about'] != null ? data.data()['about'] : '';
        imgUrl = data.data()['imgUrl'] != null ? data.data()['imgUrl'] : '';
      }
    }
  }

  nomAlert() {
    Alert(
        context: context,
        title: "Modification",
        content: Column(
          children: <Widget>[
            TextFieldWidget(
                label: "Nom",
                variable: nom,
                isNumber: false,
                fun: (value) {
                  setState(() {
                    nom = value;
                  });
                }),
            TextFieldWidget(
                label: "Prenom",
                variable: prenom,
                isNumber: false,
                fun: (value) {
                  setState(() {
                    prenom = value;
                  });
                }),
            TextFieldWidget(
                label: "Profession",
                variable: profession,
                isNumber: false,
                fun: (value) {
                  setState(() {
                    profession = value;
                  });
                }),
            TextFieldWidget(
                label: "Residence",
                variable: residence,
                isNumber: false,
                fun: (value) {
                  setState(() {
                    residence = value;
                  });
                }),
            TextFieldWidget(
                label: "Age",
                variable: age,
                isNumber: true,
                fun: (value) {
                  setState(() {
                    age = value;
                  });
                }),
            TextFieldWidget(
                variable: groupeS,
                label: "Groupe Sanguin",
                isNumber: false,
                fun: (value) {
                  setState(() {
                    groupeS = value;
                  });
                }),
          ],
        ),
        buttons: [
          DialogButton(
            onPressed: () async {
              await FirebaseFirestore.instance
                  .collection('info')
                  .doc(id_i)
                  .update({
                'nom': nom,
                'prenom': prenom,
                'residence': residence,
                'profession': profession,
                'age': age,
                'groupe_sanguin': groupeS,
              }).then((value) => Navigator.pop(context));
            },
            child: Text(
              "Mettre A jour",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ]).show();
  }

  autreInfoAlert() {
    Alert(
        context: context,
        title: "Modification",
        content: Column(
          children: <Widget>[
            TextFieldWidget(
                label: "Email",
                variable: email,
                isNumber: false,
                fun: (value) {
                  setState(() {
                    email = value;
                  });
                }),
            TextFieldWidget(
                label: "Numero 1",
                variable: num1,
                isNumber: true,
                fun: (value) {
                  setState(() {
                    num1 = value;
                  });
                }),
            TextFieldWidget(
                label: "Numero 2",
                variable: num2,
                isNumber: true,
                fun: (value) {
                  setState(() {
                    num2 = value;
                  });
                }),
            TextFormField(
              keyboardType: TextInputType.name,
              initialValue: about,
              onChanged: (value) {
                setState(() {
                  about = value;
                });
              },
              maxLines: 6,
              minLines: 2,
              decoration: InputDecoration(
                labelText: "Information supplémentaire",
              ),
            ),
          ],
        ),
        buttons: [
          DialogButton(
            onPressed: () async {
              await FirebaseFirestore.instance
                  .collection('info')
                  .doc(id_i)
                  .update({
                'email': email,
                'numero1': num1,
                'numero2': num2,
                'about': about,
              }).then((value) => Navigator.pop(context));
            },
            child: Text(
              "Mettre A jour",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ]).show();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUser();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
      // future: FirebaseAuth.instance.currentUser(User),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Scaffold(
            backgroundColor: Colors.grey.shade300,
            appBar: AppBar(
              backgroundColor: Colors.redAccent,
              title: Text('Premiers Soins'),
              actions: [
                IconButton(
                  onPressed: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.remove('email');
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext ctx) => ConnexionScreen()));
                  },
                  icon: Icon(Icons.logout),
                )
              ],
            ),
            body: SingleChildScrollView(
              child: Stack(
                children: <Widget>[
                  SizedBox(
                      height: 250,
                      width: double.infinity,
                      child: imgUrl == ''
                          ? Image(
                              image: AssetImage(
                                'images/avatar1.jpg',
                              ),
                              fit: BoxFit.cover,
                            )
                          : Image.network(
                              imgUrl,
                              fit: BoxFit.cover,
                            )),
                  Positioned(
                      top: 5,
                      right: 10,
                      child: IconButton(
                          onPressed: () {
                            pickImage();
                          },
                          icon: Icon(Icons.edit))),
                  Container(
                    margin: EdgeInsets.fromLTRB(16, 200, 16, 16),
                    child: Column(
                      children: <Widget>[
                        Stack(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(16.0),
                              margin: EdgeInsets.only(top: 16.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(left: 56.0),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        ListTile(
                                          title: Text(
                                            nom != ''
                                                ? '$nom $prenom'
                                                : 'votre nom et prenom',
                                            style:
                                                Theme.of(context).textTheme.headline6,
                                          ),
                                          trailing: IconButton(
                                            icon: Icon(Icons.edit),
                                            onPressed: () {
                                              nomAlert();
                                            },
                                          ),
                                        ),
                                        ListTile(
                                          contentPadding: EdgeInsets.all(0),
                                          title: Text(
                                            profession != ''
                                                ? '$profession'
                                                : "profession",
                                          ),
                                          subtitle: Text(
                                            residence != ''
                                                ? 'Résidence: $residence'
                                                : "Residence",
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Column(
                                          children: <Widget>[
                                            Text(age != '' ? age : "-"),
                                            Text("ans"),
                                          ],
                                        ),
                                      ),
                                      Expanded(
                                        child: Column(
                                          children: [
                                            Text(groupeS != '' ? groupeS : "-"),
                                            Text("Groupe Sanguin")
                                          ],
                                        ),
                                      ),
                                      // Expanded(
                                      //   child: Column(
                                      //     children: [
                                      //       Text("650"),
                                      //       Text("Favourites"),
                                      //     ],
                                      //   ),
                                      // ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            Container(
                              height: 60,
                              width: 60,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(60.0),
                                  image: imgUrl == ''
                                      ? DecorationImage(
                                          image: AssetImage("images/avatar1.jpg"),
                                          fit: BoxFit.cover)
                                      : DecorationImage(
                                          image: NetworkImage(imgUrl),
                                          fit: BoxFit.cover)),
                              margin: EdgeInsets.only(left: 10.0),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 20.0,
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(5.0)),
                          child: Column(
                            children: [
                              ListTile(
                                title: Text("User information"),
                                trailing: IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () => autreInfoAlert(),
                                ),
                              ),
                              Divider(),
                              ListTile(
                                title: Text("Email"),
                                subtitle:
                                    Text(email != '' ? email : "email@domaine.com"),
                                leading: Icon(Icons.mail),
                              ),
                              ListTile(
                                title: Text("Numero de personne à prevenir 1"),
                                subtitle: Text(num1 != '' ? num1 : '90 00 00 00'),
                                leading: Icon(Icons.phone),
                              ),
                              ListTile(
                                title: Text("Numero de personne à prevenir 2"),
                                subtitle: Text(num2 != '' ? num2 : '90 00 00 00'),
                                leading: Icon(Icons.phone),
                              ),
                              ListTile(
                                title: Text(
                                    "Information supplémentaire sur votre santé"),
                                subtitle: Text(about != ''
                                    ? about
                                    : "Renseigner vos informations sur votre santé, les traitements que vous suivez, ainsi que vos allergies si vous en avez"),
                                leading: Icon(Icons.person),
                              ),
                              ListTile(
                                title: Text("Joined Date"),
                                subtitle: Text("15 Juillet 2021"),
                                leading: Icon(Icons.calendar_view_day),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }
        return InitialScreen();
      },
    );
  }
}

class TextFieldWidget extends StatefulWidget {
  const TextFieldWidget({
    Key key,
    @required this.label,
    @required this.variable,
    this.fun,
    this.isNumber,
    this.maxLine,
  }) : super(key: key);
  final label;
  final variable;
  final isNumber;
  final fun;
  final maxLine;

  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: widget.isNumber ? TextInputType.number : TextInputType.name,
      controller: TextEditingController(text: widget.variable),
      onChanged: (value) => widget.fun(value),
      maxLines: widget.maxLine,
      decoration: InputDecoration(
        labelText: widget.label,
      ),
    );
  }
}
