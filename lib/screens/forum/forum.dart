import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:tutore/components/initialScreen.dart';
// import 'package:tutore/screens/login/loginScreen.dart';
// import 'package:tutore/utils/helper.dart';

class Forum extends StatefulWidget {
  static const String routeName = '/forumScreen';
  const Forum({ Key key }) : super(key: key);

  @override
  _ForumState createState() => _ForumState();
}

class _ForumState extends State<Forum> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User>(
      // future: FirebaseAuth.instance.currentUser(User),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Container();
        }
        return InitialScreen();
      },
    );
  }
}