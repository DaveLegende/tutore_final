import 'package:flutter/material.dart';
import 'package:tutore/screens/forum/forum.dart';
import 'package:tutore/screens/splash/splashScreen.dart';

import 'screens/category/category.dart';
import 'screens/login/loginScreen.dart';
import 'screens/signUp/sign_up.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        textTheme: TextTheme(
          headline6: TextStyle(
            color: Color(0xFF607D8B),
            fontWeight: FontWeight.bold
          ),
          headline5: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 15,
          ),
          headline4: TextStyle(
            color: Color(0xFF030000F),
            fontWeight: FontWeight.bold
          ),
          headline3: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold
          ),
        ),
        primarySwatch: Colors.blueGrey,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
      routes: {
        SplashScreen.routeName: (context) => SplashScreen(),
        Category.routeName: (context) => Category(),
        Forum.routeName: (context) => Forum(),
        InscriptionScreen.routeName: (context) => InscriptionScreen(),
        ConnexionScreen.routeName: (context) => ConnexionScreen()
      },
    );
  }
}

