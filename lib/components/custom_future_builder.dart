import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tutore/utils/constants.dart';
import 'package:tutore/utils/helper.dart';

import 'custom_triangle.dart';
import 'type_card_list.dart';

class CustomFutureBuilder extends StatelessWidget {
  const CustomFutureBuilder({
    @required String collectionTitle,
    @required String docTitle,
    @required Object object,
    Key key,
  }) : _collectionTitle = collectionTitle, _docTitle = docTitle, _object = object, super(key: key);

  final String _collectionTitle;
  final String _docTitle;
  final Object _object;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot<Map<String, dynamic>>>(
      future: FirebaseFirestore.instance.collection("types").doc(_docTitle).collection(_collectionTitle).get(),
      builder: (context, snapshot) {
        if(snapshot.connectionState == ConnectionState.waiting) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        // simple = TypeModel(title: snapshot.data.docs[0]['title'], desc: snapshot.data.docs[0]['desc'], image: snapshot.data.docs[0]['image']);
        // saignement = TypeModel(title: snapshot.data.docs[1]['title'], desc: snapshot.data.docs[1]['desc'], image: snapshot.data.docs[1]['image']);
        // oeil = TypeModel(title: snapshot.data.docs[2]['title'], desc: snapshot.data.docs[2]['desc'], image: snapshot.data.docs[2]['image']);
        // section = TypeModel(title: snapshot.data.docs[3]['title'], desc: snapshot.data.docs[3]['desc'], image: snapshot.data.docs[3]['image']);
        return Container(
          height: Helper.getScreenHeight(context),
          width: Helper.getScreenWidth(context),
          child: Stack(
            children: [
              Container(
                width: 100,
                height: Helper.getScreenHeight(context),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topRight: Radius.circular(100), bottomRight: Radius.circular(50)),
                  color: mPrimaryColor
                ),
              ),
              ListView.builder(
                itemCount: snapshot.data.docs.length,
                itemBuilder: (context, i) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (context) => _object)
                        );
                      },
                      child: TypeListCard(
                        title: snapshot.data.docs[i]['title'],
                        desc: snapshot.data.docs[i]['desc'],
                        imageShape: ClipPath(
                          clipper: CustomTriangle(),
                          child: Container(
                            width: 80,
                            height: 80,
                            child: Image.asset(Helper.getAssetName("dessert.jpg"), fit: BoxFit.cover),
                          ),
                        )
                      )
                    )
                  );
                },
              )
            ],
          ),
        );
      },
    );
  }
}