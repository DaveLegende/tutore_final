import 'package:flutter/material.dart';
import 'package:tutore/utils/helper.dart';
import 'package:tutore/utils/constants.dart';

class TypeListCard extends StatelessWidget {
  const TypeListCard({
    Key key,
    @required String title,
    @required String desc,
    @required Widget imageShape,
  }) : _title = title, _desc = desc, _imageShape = imageShape, super(key: key);

  final String _title;
  final String _desc;
  final Widget _imageShape;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 80,
          padding: const EdgeInsets.symmetric(horizontal: 80),
          margin: const EdgeInsets.symmetric(horizontal: 20),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(50),
              bottomLeft: Radius.circular(50),
              topRight: Radius.circular(10),
              bottomRight: Radius.circular(10),
            ),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: placeholder,
                offset: Offset(0,5),
                blurRadius: 10
              )
            ]
          ),
          child: ListTile(
            title: Text(_title, /*style: Helper.getTheme(context).headline4.copyWith(color: placeholder )*/),
            subtitle: Text("$_desc",
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          // Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     Text(_title, /*style: Helper.getTheme(context).headline4.copyWith(color: placeholder )*/),
          //     SizedBox(height: 5),
          //     Text("$_desc")
          //   ]
          // ),
        ),
        SizedBox(
          height: 80,
          child: Align(
            alignment: Alignment.centerLeft,
            child: _imageShape,
          )
        ),
        SizedBox(
          height: 80,
          child: Align(
            alignment: Alignment.centerRight,
            child: Container(
              width: 40,
              height: 40,
              decoration: ShapeDecoration(
                shape: CircleBorder(),
                color: Colors.white,
                shadows: [
                  BoxShadow(
                    color: placeholder,
                    offset: Offset(0,2),
                    blurRadius: 5
                  )
                ]
              ),
              child: Image.asset(Helper.getAssetName("next_filled.png")),
            ),
          )
        )
      ],
    );
  }
}
