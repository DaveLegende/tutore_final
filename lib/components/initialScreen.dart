import 'package:flutter/material.dart';
import 'package:tutore/screens/signUp/sign_up.dart';
import 'package:tutore/utils/helper.dart';

class InitialScreen extends StatelessWidget {
  const InitialScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: Helper.getScreenWidth(context),
          height: Helper.getScreenHeight(context),
          child: Image.asset(
            Helper.getAssetName("splashIcon.png"),
            fit: BoxFit.cover,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 100, horizontal: 30),
          child: Column(
            children: [
              Container(
                width: 100,
                height: 100,
                child: Image.asset(
                  Helper.getAssetName("emoji.gif"),
              )),
              Row(
                children: [
                  Text("Désolé vous devez creer un compte.",
                    // style: Helper.getTheme(context).headline5,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, InscriptionScreen.routeName);
                    },
                    child: Text("S'inscrire ?",
                      style: Helper.getTheme(context).headline5,
                    ),
                  )
                ],
              )
            ],
          ),
        )
      ],
    );
  }
}