import 'package:flutter/material.dart';
import 'package:tutore/utils/constants.dart';

class CustomTextInput extends StatelessWidget {
  const CustomTextInput({
    @required String hintText,
    Key key,
  }) : _hintText = hintText, super( key: key);

  final String _hintText;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 50,
      decoration: ShapeDecoration(
        color: mcardColor,
        shape: StadiumBorder(),
      ),
      child: TextFormField(
        decoration: InputDecoration(
            hintText: _hintText,
            hintStyle: TextStyle(color: placeholder),
            border: InputBorder.none,
            contentPadding: const EdgeInsets.only(left: 40)),
      ),
    );
  }
}
