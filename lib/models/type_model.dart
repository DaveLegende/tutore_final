import 'package:flutter/material.dart';

class TypeModel {
  final String image;
  final String title;
  final String desc;

  TypeModel({
    @required this.image,
    @required this.title,
    @required this.desc,
  });
}