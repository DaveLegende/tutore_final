
import 'package:firebase_auth/firebase_auth.dart';

class AuthServices {
  FirebaseAuth auth = FirebaseAuth.instance;

  Future<bool> signUp(String email, String password) async {
    try {
      final result = await auth.createUserWithEmailAndPassword(email: email, password: password);
      if(result.user != null) return true;
      return false;
    } catch (e) {
      return false;
    }
  }
}